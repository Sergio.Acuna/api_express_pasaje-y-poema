import express from "express";

//crear objeto llamado app
let app=express();

//Crear rutas 

app.get("/poema", (req, res)=>{


res.send("El despertar<br><br> A León Ostrov<br><br> Señor<br>La jaula se ha vuelto pájaro<br>y se ha volado<br>y mi corazón está loco<br>porque aúlla a la muerte<br>y sonríe detrás del viento<br>a mis delirios<br><br>");

});

app.get("/pasaje", (req, res)=>{


    res.send("Isaías 43.1-2<br><br>Pero ahora, así dice el Señor,<br>el que te creó, Jacob,<br>el que teformó, Israel:<br>«No temas, que yo te he redimido;<br>te he llamado por tu nombre; tú eres mío.<br>Cuando cruces las aguas,<br>yo estaré contigo;<br>cuando cruces los ríos,<br>no te cubrirán sus aguas;<br>cuando camines por el fuego,<br>no te quemarás ni te abrasarán las llamas.»");
    
    });

app.listen(3000);

/*
import express from 'express';
//crear objeto llamado app
const app = express();
//constante numerica
const puerto = 3000;

//Ruta: Punto de entrada a mi aplicación
//URL donde nuestra aplicación va a responder

//Función callback
//Es una función que se pasa como argumento al momento de llamar otra función
//Se ejecuta cuando la función a la que llame se termina de ejecutar 

function poema(peticion, respuesta){

    respuesta.send("El despertar<br><br> A León Ostrov<br><br> Señor<br>La jaula se ha vuelto pájaro<br>y se ha volado<br>y mi corazón está loco<br>porque aúlla a la muerte<br>y sonríe detrás del viento<br>a mis delirios<br><br>");

}

app.get("/poema", poema);//requiere 2 argumentos 

function pasaje(peticion, respuesta){

    respuesta.send("Isaías 43.1-2<br><br>Pero ahora, así dice el Señor,<br>el que te creó, Jacob,<br>el que teformó, Israel:<br>«No temas, que yo te he redimido;<br>te he llamado por tu nombre; tú eres mío.<br>Cuando cruces las aguas,<br>yo estaré contigo;<br>cuando cruces los ríos,<br>no te cubrirán sus aguas;<br>cuando camines por el fuego,<br>no te quemarás ni te abrasarán las llamas.»");

}

app.get("/pasaje", pasaje);//requiere 2 argumentos 

app.listen


*/